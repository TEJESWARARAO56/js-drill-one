let inventory = require('./data.cjs');

function getCarRelasingYears(inventory) {
    if (inventory === []) {
        return []
    }
    let carYears = [];
    for (let index = 0; index < inventory.length; index++) {
        carYears.push(inventory[index]['car_year']);
    }
    console.log(carYears);
    return carYears;
}

if (require.main === module) {
    getCarRelasingYears(inventory);
}

module.exports = getCarRelasingYears;



/* ==== Problem #4 ====
The accounting team needs all the years from every car on the lot. 
Execute a function that will return an array from the dealer data containing only the car years 
and log the result in the console as it was returned.

getCarRelasingYears(inventory); */