let inventory = require('./data.cjs');

function problemOne(inventory, searchid) {
    // console.log(typeof inventory, inventory)

    if (inventory === undefined || searchid === undefined || inventory instanceof String) {
        return []
    } else if (
        typeof inventory === 'object' &&
        !Array.isArray(inventory) &&
        inventory !== null
    ) {
        return []
    }

    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index]["id"] === searchid) {
            // console.log(`Car 33 is a ${inventory[index]['car_year']} ${inventory[index]['car_make']} ${inventory[index]['car_model']}`);
            return [inventory[index]]
        }
    }
    return []
}

if (require.main === module) {
    const result = problemOne({ id: 33, name: "Test", length: 10 }, 33);
    // const result = problemOne(new String("hello"), 33);


    console.log(result)
}

module.exports = problemOne
/* ==== Problem #1 ====
The dealer can't recall the information for a car with an id of 33 on his lot. 
Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. 
Then log the car's year, make, and model in the console log in the format of:

"Car 33 is a *car year goes here* *car make goes here* *car model goes here*" */




