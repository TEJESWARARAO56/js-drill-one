const inventory = require('./data.cjs');

function getBmwAudi(inventory) {
    if (inventory === []) {
        return [];
    }
    let bmwAndAudi = [];
    for (let index = 0; index < inventory.length; index++) {
        if (["BMW", "Audi"].includes(inventory[index]["car_make"])) {
            bmwAndAudi.push(inventory[index]);
        }
    }
    console.log(bmwAndAudi);
    return bmwAndAudi;
}

if (require.main === module) {
    getBmwAudi(inventory);
}

module.exports = getBmwAudi;
/* ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  
Execute a function and return an array that only contains BMW and Audi cars.  
Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.*/