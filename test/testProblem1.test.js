let inventory=require('../data.cjs');
let problem1=require('../problem1.cjs')
let searchid=33

test('checking data of car id 33', () => {
  const result = problem1(inventory,searchid);
  expect(result).toStrictEqual({ id: 33, car_make: 'Jeep', car_model: 'Wrangler', car_year: 2011 });
});

