let inventory = require('./data.cjs')

function problemTwo(inventory) {
    if (inventory === undefined || searchid === undefined) {
        return []
    } else if (
        typeof inventory === 'object' &&
        !Array.isArray(inventory) &&
        inventory !== null
    ) {
        return inventory
    }
    let lastIndex = inventory.length - 1;
    console.log(`Last car is a ${inventory[lastIndex]['car_year']} ${inventory[lastIndex]['car_make']} ${inventory[lastIndex]['car_model']}`);
    return inventory[lastIndex];
}

if (require.main === module) {
    problemTwo(inventory);
}

module.exports = problemTwo;


/*  ==== Problem #2 ====
The dealer needs the information on the last car in their inventory. 
Execute a function to find what the make and model of the last car in the inventory is?  
Log the make and model into the console in the format of:

"Last car is a *car make goes here* *car model goes here*" */
